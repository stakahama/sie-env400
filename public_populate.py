#!/usr/bin/env python

## Create project homepage for GitLab pages

## -----------------------------------------------------------------------------

import platform
import re
import shutil
import subprocess
import glob

## -----------------------------------------------------------------------------

if re.search('MSYS_NT|Windows', platform.system()):
    pandoc_exe = '"C:/Program Files/RStudio/bin/pandoc/pandoc.exe"'
else:
    pandoc_exe = 'pandoc'

cmd = '{pandoc_exe} -s --self-contained \
-f markdown \
-t html5 \
-o public/index.html \
README.md'.format(pandoc_exe = pandoc_exe)

print(cmd)
subprocess.call(cmd, shell=True)

## -----------------------------------------------------------------------------

if False: # don't need since 'self-contained'
    
    path_fig = 'figures'
    file_fig = 'NABEL_Network.png'

    if not os.path.exists(os.path.join('public', path_fig)):
        os.makedirs(os.path.join('public', path_fig))

    if not os.path.exists(os.path.join('public', path_fig, file_fig)):
        shutil.copy2(os.path.join('contents', path_fig, file_fig),
                     os.path.join('public', path_fig, file_fig))

## -----------------------------------------------------------------------------

if False:
    for ff in glob.glob('contents/*.html') + glob.glob('contents/*.R'):
        shutil.copy2(ff, 'public')
