1. <a href="http://rawgit.com/stakahama/aprl-env400-assignment/master/contents/01_Rintro.html" target="_blank">Motivating Example</a>
2. <a href="http://rawgit.com/stakahama/aprl-env400-assignment/master/contents/02_projectdef.html" target="_blank">Assignment definition</a>
3. <a href="http://rawgit.com/stakahama/aprl-env400-assignment/master/contents/03_Rbasics.html" target="_blank">R basics</a>
4. <a href="http://rawgit.com/stakahama/aprl-env400-assignment/master/contents/04_tseriesviz.html" target="_blank">Visualizing time series</a>
5. <a href="http://rawgit.com/stakahama/aprl-env400-assignment/master/contents/05_correlations.html" target="_blank">Correlations and cross-correlations</a>
6. <a href="http://rawgit.com/stakahama/aprl-env400-assignment/master/contents/06_signal.html" target="_blank">Autocorrelation and periodicity</a>
7. <a href="http://rawgit.com/stakahama/aprl-env400-assignment/master/contents/07_stochastic.html" target="_blank">Stochastic processes and random variables</a>
8. <a href="http://rawgit.com/stakahama/aprl-env400-assignment/master/contents/08_inferential.html" target="_blank">Inferential statistics and hypothesis testing</a>
9. <a href="http://rawgit.com/stakahama/aprl-env400-assignment/master/contents/09_extremevals.html" target="_blank">Extreme values: detection and accommodation</a>
10. <a href="http://rawgit.com/stakahama/aprl-env400-assignment/master/contents/10_wind.html" target="_blank">Considering meteorology (wind directions)</a>
